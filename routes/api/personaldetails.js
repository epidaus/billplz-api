const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const PersonalDetails = require('../../model/PersonalDetails');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const personalDetails = await PersonalDetails.create({
        user: req.params.id,
        job_title: req.body.job_title,
        fname: req.body.fname,
        lname: req.body.lname,
        email: req.body.email,
        phone: req.body.phone,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { personalDetails: personalDetails._id }
        },  {useFindAndModify: false})

      res.status(202).json(personalDetails)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const personalDetails = await PersonalDetails.find({
        user: req.params.id
    })
    res.status(200).json(personalDetails)
});

router.delete('/delete/:id', async (req, res) => {
    const personalDetails = await PersonalDetails.findById({
        _id: req.params.id
    })

    await PersonalDetails.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: personalDetails.user._id
    }, {
      $pull: { personalDetails: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await PersonalDetails.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const personalDetails = await PersonalDetails.findById(req.params.id).populate('user', 'name')

    res.json(personalDetails)
})

module.exports = router;