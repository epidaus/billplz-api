const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Language = require('../../model/Language');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const language = await Language.create({
        user: req.params.id,
        title: req.body.title,
        score: req.body.score,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { language: language._id }
        },  {useFindAndModify: false})

      res.status(202).json(language)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const language = await Language.find({
        user: req.params.id
    })
    res.status(200).json(language)
});

router.delete('/delete/:id', async (req, res) => {
    const language = await Language.findById({
        _id: req.params.id
    })

    await Language.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: language.user._id
    }, {
      $pull: { language: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Language.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const language = await Language.findById(req.params.id).populate('user', 'name')

    res.json(language)
})

module.exports = router;