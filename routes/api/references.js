const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Reference = require('../../model/References');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const reference = await Reference.create({
        user: req.params.id,
        name: req.body.name,
        title: req.body.title,
        company: req.body.company,
        email: req.body.email,
        phone: req.body.phone,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { reference: reference._id }
        },  {useFindAndModify: false})

      res.status(202).json(reference)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const reference = await Reference.find({
        user: req.params.id
    })
    res.status(200).json(reference)
});

router.delete('/delete/:id', async (req, res) => {
    const reference = await Reference.findById({
        _id: req.params.id
    })

    await Reference.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: reference.user._id
    }, {
      $pull: { reference: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Reference.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const reference = await Reference.findById(req.params.id).populate('user', 'name')

    res.json(reference)
})

module.exports = router;