const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Employment = require('../../model/EmploymentHistory');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const employment = await Employment.create({
        user: req.params.id,
        startdate: req.body.startdate,
        enddate: req.body.enddate,
        title: req.body.title,
        company_name: req.body.company_name,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { employment: employment._id }
        },  {useFindAndModify: false})

      res.status(202).json(employment)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const employment = await Employment.find({
        user: req.params.id
    })
    res.status(200).json(employment)
});

router.delete('/delete/:id', async (req, res) => {
    const employment = await Employment.findById({
        _id: req.params.id
    })

    await Employment.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: employment.user._id
    }, {
      $pull: { employment: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Employment.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const employment = await Employment.findById(req.params.id).populate('user', 'name')

    res.json(employment)
})

module.exports = router;