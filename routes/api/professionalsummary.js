const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Summary = require('../../model/Summary');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const summary = await Summary.create({
        user: req.params.id,
        summary_desc: req.body.summary_desc,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { summary: summary._id }
        },  {useFindAndModify: false})

      res.status(202).json(summary)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const summary = await Summary.find({
        user: req.params.id
    })
    res.status(200).json(summary)
});

router.delete('/delete/:id', async (req, res) => {
    const summary = await Summary.findById({
        _id: req.params.id
    })

    await Summary.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: summary.user._id
    }, {
      $pull: { summary: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Summary.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const summary = await Summary.findById(req.params.id).populate('user', 'name')

    res.json(summary)
})

module.exports = router;