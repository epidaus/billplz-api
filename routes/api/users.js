const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const passport = require('passport');
const key = require('../../config/keys').secret;

const Task = require('../../model/Task');
const User = require('../../model/User');

//Register
router.post('/register', async (req, res) => {
    let {
        name,
        username,
        email,
        password,
        confirm_password,
        phone,
        dob,
        address,
        status
    } = req.body
    if (password !== confirm_password) {
        return res.status(400).json({
            msg: "Password do not match."
        });
    }

    const user = await User.findOne({ username: username })
    const emailUser = await User.findOne({ email: email })

    if (user && emailUser) {
        return res.status(400).json({
            success: false,
            msg: "Username and Email Already Exist"
        });
    }

    if (user) {
        return res.status(400).json({
            success: false,
            msg: "Username Already Exist"
        });
    }

    if (emailUser) {
        return res.status(400).json({
            success: false,
            msg: "Email Already Exist"
        });
    }

    let newUser = new User({
        name,
        username,
        password,
        email,
        phone,
        dob,
        address,
        status
    });
    bcrypt.genSalt(10, (err, salt) => {
        bcrypt.hash(newUser.password, salt, (err, hash) => {
            if (err) throw err;
            newUser.password = hash;
            newUser.save().then(user => {
                return res.status(201).json({
                    success: true,
                    msg: "Successfully Registered."
                });
            });
        });
    });
});


//Login
router.post('/login', (req, res) => {
    User.findOne({
        username: req.body.username
    }).then(user => {
        if (!user) {
            return res.status(404).json({
                msg: "Username not found.",
                success: false
            });
        }
        bcrypt.compare(req.body.password, user.password).then(isMatch => {
            if (isMatch) {
                const payload = {
                    _id: user._id,
                    username: user.username,
                    name: user.name,
                    email: user.email
                }
                jwt.sign(payload, key, {
                    expiresIn: 604800
                }, (err, token) => {
                    res.status(200).json({
                        success: true,
                        token: `Bearer ${token}`,
                        user: user,
                        msg: "You are now logged in."
                    });
                })
            } else {
                return res.status(404).json({
                    msg: "Incorrect password.",
                    success: false
                });
            }
        })
    });
});


//View Profile
router.get('/profile', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    return res.json({
        user: req.user
    });
});

router.get('/user/:id', passport.authenticate('jwt', {
    session: false
}), (req, res) => {
    User.findById(req.params.id).then((response) => {
        if(!response) {
            return res.status(404).end()
        }
        return res.status(200).json(response)
    })
})

//List all Users
router.get('/userlist', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {

    const user = await User.find({}).populate('task')
    res.status(200).json(user)
  });

//Delete
router.delete('/delete/:id', async (req, res) => {
    try {
        await User.deleteOne({
            _id: req.params.id
        }).then((response) => {
            if(response.deletedCount === 1){
                return res.status(200).json({
                    msg : 'Successfully Deleted!'
                })
            } else {
                return res.status(401).json({
                    msg : 'User not exist!'
                })
            }
        });
        
    } catch (error) {
        return res.status(400).json({
            msg: 'User not exist!'
        }); 
    }
});

//Add Profile Info
router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
  await User.updateOne({ 
      _id: req.params.id
    }, 
    { 
        ...req.body 
    }, 
    function(err, result) {
    if (err) {
      res.send(err);
    } else {
      res.json(result);
    }
    });
});

module.exports = router;