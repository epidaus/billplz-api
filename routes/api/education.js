const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Education = require('../../model/Education');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const education = await Education.create({
        user: req.params.id,
        startdate: req.body.startdate,
        enddate: req.body.enddate,
        school: req.body.school,
        title: req.body.title,
        result: req.body.result,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { education: education._id }
        },  {useFindAndModify: false})

      res.status(202).json(education)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const education = await Education.find({
        user: req.params.id
    })
    res.status(200).json(education)
});

router.delete('/delete/:id', async (req, res) => {
    const education = await Education.findById({
        _id: req.params.id
    })

    await Education.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: education.user._id
    }, {
      $pull: { education: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Education.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const education = await Education.findById(req.params.id).populate('user', 'name')

    res.json(education)
})

module.exports = router;