const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Task = require('../../model/Task');

router.post('/addtask/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    
    const task = await Task.create({
        user: req.params.id,
        title: req.body.title,
        desc: req.body.desc,
        status: req.body.status,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { task: task._id }
        },  {useFindAndModify: false})

      res.status(202).json(task)
     
})

router.get('/task-list', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const task = await Task.find({}).populate('user', 'name').sort({date: 'descending'})
    res.status(200).json(task)
});

router.get('/task-list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const task = await Task.find({
        user: req.params.id
    })
    res.status(200).json(task)
});


router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const task = await Task.findById(req.params.id).populate('user', 'name')

    res.json(task)
})


router.delete('/delete/:id', async (req, res) => {
    const task = await Task.findById({
        _id: req.params.id
    })

    await Task.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: task.user._id
    }, {
      $pull: { task: { $in: req.params.id } }
    },  {useFindAndModify: false})

    

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Task.updateOne({ 
        _id: req.params.id
        },
        { 
        ...req.body 
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})


module.exports = router;