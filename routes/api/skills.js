const express = require('express');
const router = express.Router();
const passport = require('passport');

const User = require('../../model/User');
const Skill = require('../../model/Skills');

router.post('/add/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const skill = await Skill.create({
        user: req.params.id,
        title: req.body.title,
        score: req.body.score,
        createdAt: new Date()
      }
      )

        await User.findOneAndUpdate({
            _id: req.params.id
        }, {
          $push: { skill: skill._id }
        },  {useFindAndModify: false})

      res.status(202).json(skill)
     
})

router.get('/list/:id', passport.authenticate('jwt', {
    session: false
}), async function(req, res) {
    const skill = await Skill.find({
        user: req.params.id
    })
    res.status(200).json(skill)
});

router.delete('/delete/:id', async (req, res) => {
    const skill = await Skill.findById({
        _id: req.params.id
    })

    await Skill.deleteOne({
        _id: req.params.id
    })

    await User.findOneAndUpdate({
        _id: skill.user._id
    }, {
      $pull: { skill: { $in: req.params.id } }
    },  {useFindAndModify: false})

    res.status(200).json({
        msg: 'Successfully Deleted!'
    })
    
});

router.put('/update/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    await Skill.updateOne({
        _id: req.params.id
        },
        { 
        ...req.body
        },
        function(err, result) {
            if (err) {
              res.send(err);
            } else {
              res.json(result);
            }
        })
})

router.get('/:id', passport.authenticate('jwt', {
    session: false
}), async (req, res) => {
    const skill = await Skill.findById(req.params.id).populate('user', 'name')

    res.json(skill)
})

module.exports = router;