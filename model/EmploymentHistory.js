const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const EmploymentSchema = new Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users',
        required: true
    },
    startdate: {
        type: String,
        required: true
    },
    enddate: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    company_name: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: Date.now
    }
});

module.exports = Employment = mongoose.model('employment', EmploymentSchema);